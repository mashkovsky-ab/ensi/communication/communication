<table class="spacer" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
        <th height="{{ $size }}" class="spacer__cell " style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: {{ $size }}px; font-weight: normal; line-height: {{ $size }}px; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center;">&nbsp;</th>
    </tr>
</table>
