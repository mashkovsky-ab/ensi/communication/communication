<table class="email-order-summary" align="right" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: auto;">
    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
        <th class="container__cell" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 24px; padding-right: 24px; padding-top: 0; text-align: center;">
            <table style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
                <tr class="email-order-summary__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-summary__cell email-order-summary__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: right;">
                        Количество товаров
                    </th>
                    <th class="email-order-summary__cell email-order-summary__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 24px; padding-right: 0; padding-top: 0; text-align: right;">
                        {{ $total_count }}
                    </th>
                </tr>
                <tr class="email-order-summary__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-summary__cell email-order-summary__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: right;">
                        Сумма товаров
                    </th>
                    <th class="email-order-summary__cell email-order-summary__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 24px; padding-right: 0; padding-top: 0; text-align: right;">
                        {{ $items_cost }}&nbsp;руб.
                    </th>
                </tr>
                <tr class="email-order-summary__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-summary__cell email-order-summary__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: right;">
                        Доставка
                    </th>
                    <th class="email-order-summary__cell email-order-summary__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 24px; padding-right: 0; padding-top: 0; text-align: right;">
                        {{ $delivery_cost }}&nbsp;руб.
                    </th>
                </tr>
                <tr class="email-order-summary__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-summary__cell email-order-summary__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: right;">
                        Всего к&nbsp;оплате
                    </th>
                    <th class="email-order-summary__cell email-order-summary__cell--total" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 18px; font-weight: bold; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 24px; padding-right: 0; padding-top: 0; text-align: right;">
                        {{ $order_cost }}&nbsp;руб.
                    </th>
                </tr>
            </table>
        </th>
    </tr>
</table>
