@php
/** @var string $code */
@endphp

Здравствуйте {{ $first_name }} {{ $middle_name }}, <br>
Администратор платформы Ensi опубликовал новый Акт о реферальных зачислениях в Вашем личном кабинете.
<br><br>
<b>Копия акта №{{ $document_id }} представлена ниже:</b>
<br><br>
Период зачисления: {{ $document_period_since }} — {{ $document_period_to }}<br>
Сумма вознаграждения: {{ $document_amount_reward }} руб.<br>
Статус документа: <em>{{ $document_status }}</em><br>
Дата документа: {{ $document_creation_date }}<br>
@if ($file_url)
Актуальная версия файла доступна по ссылке: <a href="{{ $file_url }}">{{ $file_url }}</a>
@endif
<br><br>
С уважением,<br>
— Администрация Ensi.
