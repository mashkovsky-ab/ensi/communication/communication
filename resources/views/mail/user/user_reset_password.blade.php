@php
    /** @var string $fullName */
    /** @var string $link */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.users.reset_password.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ __('messages.users.reset_password.link_prefix') }} <a href="{{ $link }}" style=color:#046a38>{{ __('messages.users.reset_password.link_body') }}</a>.
</p>

