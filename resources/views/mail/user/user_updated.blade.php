@php
    /** @var string $fullName */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.users.user_updated.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ __('messages.users.user_updated.message_body') }}
</p>

