@php
    /** @var string $fullName */
    /** @var string $causeDeactivation */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.users.user_deactivated.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ __('messages.users.user_deactivated.message_body', ['causeDeactivation' => $causeDeactivation]) }}
</p>

