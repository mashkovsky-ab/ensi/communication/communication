@php
    /** @var string $fullName */
    /** @var string $link */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.users.reset_password_success.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ __('messages.users.reset_password_success.message_body') }}
</p>
