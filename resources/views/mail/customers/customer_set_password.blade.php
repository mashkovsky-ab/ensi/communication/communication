@php
    /** @var string $fullName */
    /** @var string $link */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.customers.set_password.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ __('messages.customers.set_password.link_prefix') }} <a href={{ $link }} style=color:#046a38>{{ __('messages.customers.set_password.link_body') }}</a>.
</p>

