@php
    /** @var string $fullName */
    /** @var array $attributes */
@endphp

@extends('layouts.email')

<p>
    {{ __('messages.customers.customers.customer_updated.title', ['fullName' => $fullName]) }}
</p>

<p>
    {{ __('messages.customers.customers.customer_updated.text') }}
</p>

<p>
    @foreach($attributes as $attributeKey => $attributeValue)
        {{ __("messages.customers.customers.customer_updated.attributes.{$attributeKey}.name") }}: {{ $attributeValue['old'] }} -> {{ $attributeValue['new'] }}<br>
    @endforeach
</p>

