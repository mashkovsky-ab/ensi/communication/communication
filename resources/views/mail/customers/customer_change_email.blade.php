@php
    /** @var string $fullName */
    /** @var string $link */
    /** @var string $newEmail */
@endphp

@extends('layouts.email')

<p>
    {{ trim(__('messages.customers.change_email.title', ['fullName' => $fullName]), ', ') . '!' }}
</p>

<p>
    {{ trim(__('messages.customers.change_email.link_prefix', ['newEmail' => $newEmail]), ', ') }} <a href={{ $link }} style=color:#046a38>{{ __('messages.customers.change_email.link_body') }}</a>.
</p>

