<?php

namespace App\Domain\Events\Mail;

class OrderPaid extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/order_paid');
    }
}
