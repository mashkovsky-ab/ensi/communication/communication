<?php

namespace App\Domain\Events\Mail;

use Illuminate\Mail\Mailable;

abstract class AbstractOrderMail extends Mailable
{
    public $order_number;
    public $create_date;
    public $receiver_name;
    public $receiver_email;
    public $receiver_phone;
    public $delivery_address;
    public $delivery_comment;
    public $delivery_cost;
    public $basket_items;
    public $items_cost;
    public $order_cost;
    public $total_count;

    public function __construct(array $data = [])
    {
        $this->subject = $data['subject'];
        $this->order_number = $data['order_number'];
        $this->create_date = $data['create_date'];
        $this->receiver_name = $data['receiver_name'];
        $this->receiver_email = $data['receiver_email'];
        $this->receiver_phone = $data['receiver_phone'];
        $this->delivery_address = $data['delivery_address'];
        $this->delivery_comment = $data['delivery_comment'] ?? '';
        $this->delivery_cost = $data['delivery_cost'];
        $this->basket_items = $data['basket_items'];
        $this->items_cost = $data['items_cost'];
        $this->order_cost = $data['order_cost'];
        $this->total_count = $data['total_count'];
    }

    abstract public function build();
}
