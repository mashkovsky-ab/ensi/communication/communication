<?php

namespace App\Domain\Events\Mail;

class ManagerOrderReturned extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/manager_order_returned');
    }
}
