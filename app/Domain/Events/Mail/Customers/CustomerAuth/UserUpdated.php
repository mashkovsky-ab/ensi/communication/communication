<?php

namespace App\Domain\Events\Mail\Customers\CustomerAuth;

use Illuminate\Mail\Mailable;

class UserUpdated extends Mailable
{
    public string $fullName;
    public array $attributes;

    public function __construct(array $data)
    {
        $this->subject = $data['subject'];

        $this->fullName = $data['full_name'];
        $this->attributes = $data['attributes'];
    }

    public function build(): self
    {
        return $this->view('mail/customers/customer_auth/user_updated');
    }
}
