<?php

namespace App\Domain\Events\Mail\Customers;

use Illuminate\Mail\Mailable;

class CustomersChangeEmail extends Mailable
{
    public string $fullName;
    public string $link;
    public string $newEmail;

    public function __construct(array $data = [])
    {
        $this->fullName = $data['full_name'];
        $this->link = $data['link'];
        $this->newEmail = $data['new_email'];
        $this->subject = $data['subject'];
    }

    public function build()
    {
        return $this->view('mail/customers/customer_change_email');
    }
}
