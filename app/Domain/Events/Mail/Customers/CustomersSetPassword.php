<?php

namespace App\Domain\Events\Mail\Customers;

use Illuminate\Mail\Mailable;

class CustomersSetPassword extends Mailable
{
    public string $fullName;
    public string $link;

    public function __construct(array $data)
    {
        $this->fullName = $data['full_name'];
        $this->link = $data['link'];
        $this->subject = $data['subject'];
    }

    public function build(): self
    {
        return $this->view('mail/customers/customers_set_password');
    }
}
