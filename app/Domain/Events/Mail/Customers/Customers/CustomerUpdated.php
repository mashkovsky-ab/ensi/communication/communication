<?php

namespace App\Domain\Events\Mail\Customers\Customers;

use Illuminate\Mail\Mailable;

class CustomerUpdated extends Mailable
{
    public string $fullName;
    public array $attributes;

    public function __construct(array $data)
    {
        $this->subject = $data['subject'];

        $this->fullName = $data['full_name'];
        $this->attributes = $data['attributes'];
    }

    public function build(): self
    {
        return $this->view('mail/customers/customers/customer_updated');
    }
}
