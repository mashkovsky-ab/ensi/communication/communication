<?php

namespace App\Domain\Events\Mail;

class OrderReturned extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/order_returned');
    }
}
