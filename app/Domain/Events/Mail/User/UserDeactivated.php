<?php

namespace App\Domain\Events\Mail\User;

use Illuminate\Mail\Mailable;

class UserDeactivated extends Mailable
{
    public string $fullName;
    public string $causeDeactivation;

    public function __construct(array $data)
    {
        $this->fullName = $data['full_name'];
        $this->causeDeactivation = $data['cause_deactivation'];
        $this->subject = $data['subject'];
    }

    public function build(): self
    {
        return $this->view('mail/user/user_deactivated');
    }
}
