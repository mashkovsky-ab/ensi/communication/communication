<?php

namespace App\Domain\Events\Mail\User;

use Illuminate\Mail\Mailable;

class UserChangeEmail extends Mailable
{
    public $code;
    public $subject = 'Запрос на смену email';

    public function __construct(array $data = [])
    {
        $this->code = $data['code'];
    }

    public function build()
    {
        return $this->view('mail/user/user_change_email');
    }
}
