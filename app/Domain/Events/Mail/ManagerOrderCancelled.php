<?php

namespace App\Domain\Events\Mail;

class ManagerOrderCancelled extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/manager_order_cancelled');
    }
}
