<?php

namespace App\Domain\Events\Actions;

use App\Exceptions\UndefinedEventException;
use App\Http\ApiV1\Requests\EventRequest;

class ParseEventAction
{
    public function __construct(
        private ValidateInternalMessageAction $validateInternalMessageAction,
        private ValidateEmailMessageAction $validateEmailMessageAction
    ) {
    }

    public function execute(EventRequest $request)
    {
        $eventType = $request->get('type');

        switch ($eventType) {
            case 'send-internal-message':
                $this->validateInternalMessageAction->execute($request);

                return true;
            case 'send-email-message':
                $this->validateEmailMessageAction->execute($request);

                return true;
            default:
                throw new UndefinedEventException('Undefined event', 400);
        }
    }
}
