<?php

namespace App\Domain\Events\Actions;

class ValidateEmailMessageAction
{
    public function __construct(private SendEmailAction $sendEmailAction)
    {
    }

    public function execute($request)
    {
        $data = validator($request->get('data'), [
            'text' => ['required', 'string'],
            'subject' => ['required', 'string'],
            'to' => ['required', 'array'],
            'to.*' => ['required', 'email'],
        ])->validate();

        $allRecipients = collect($data['to']);
        $type = 'plain';

        foreach ($allRecipients->chunk(10) as $chunk) {
            $recipients = array_values($chunk->all());
            $this->sendEmailAction->onQueue()->execute($recipients, $type, $data);
        }

        return true;
    }
}
