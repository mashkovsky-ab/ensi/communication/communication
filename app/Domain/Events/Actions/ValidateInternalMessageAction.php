<?php

namespace App\Domain\Events\Actions;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use Illuminate\Validation\Rule;

class ValidateInternalMessageAction
{
    public function __construct(private SendInternalMessageAction $sendInternalMessageAction)
    {
    }

    public function execute($request)
    {
        $data = validator($request->get('data'), [
            'text' => ['required', 'string'],
            'chat_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'files' => ['nullable', 'array'],
            'user_type' => ['required', Rule::in(UserTypeEnum::cases())],
        ])->validate();

        $this->sendInternalMessageAction->onQueue()->execute($data);

        return true;
    }
}
