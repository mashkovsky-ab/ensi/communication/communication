<?php

namespace App\Domain\Events\Actions\Units\AdminAuth\SendEmail;

use App\Domain\Events\Mail\User\UserDeactivated;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\MessageAboutDeactivationUser;
use Ensi\AdminAuthClient\ObjectSerializer;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class DeactivatedAction
{
    public function __construct(private UsersApi $usersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageAboutDeactivationUser $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageAboutDeactivationUser::class);
        $user = $this->usersApi->getUser($message->getUserId())->getData();

        Mail::to($user->getEmail())->send(new UserDeactivated([
            'full_name' => $user->getFullName(),
            'cause_deactivation' => $message->getCauseDeactivation(),
            'subject' => __('messages.users.user_deactivated.subject'),
        ]));
    }
}
