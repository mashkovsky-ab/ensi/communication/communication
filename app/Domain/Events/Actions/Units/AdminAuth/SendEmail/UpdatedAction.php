<?php

namespace App\Domain\Events\Actions\Units\AdminAuth\SendEmail;

use App\Domain\Events\Mail\User\UserUpdated;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\MessageAboutUpdationUser;
use Ensi\AdminAuthClient\ObjectSerializer;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class UpdatedAction
{
    public function __construct(private UsersApi $usersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageAboutUpdationUser $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageAboutUpdationUser::class);
        $user = $this->usersApi->getUser($message->getUserId())->getData();

        Mail::to($user->getEmail())->send(new UserUpdated([
            'full_name' => $user->getFullName(),
            'subject' => __('messages.users.user_updated.subject'),
        ]));
    }
}
