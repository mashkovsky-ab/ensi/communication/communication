<?php

namespace App\Domain\Events\Actions\Customers;

use App\Domain\Events\Mail\User\UserResetPassword;
use Ensi\CustomerAuthClient\Dto\MessageForResetPassword;
use Ensi\CustomerAuthClient\ObjectSerializer;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;
use Vonage\SMS\Message\SMS;
use Vonage\Client\Credentials\Basic;
use Vonage\Client;

class ResetPasswordAction
{
    public function __construct(private UsersApi $usersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageForResetPassword $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForResetPassword::class);

        if ($message->getUserPhone()) {
            $basicCredentials  = new Basic(config('vonage-client.api_key'), config('vonage-client.api_secret'));
            $vonageClient = new Client($basicCredentials);

            $response = $vonageClient->sms()->send(
                new SMS($message->getUserPhone(), config('app.name'), 'Введите код ' . $message->getConfirmationCode() . ' для сброса пароля.')
            );
        } else {
            Mail::to($message->getUserEmail())->send(new UserResetPassword([
                'full_name' => $message->getUserFullName(),
                'link' => config('internal_services.front_urls.reset_password')  . $message->getToken(),
                'subject' => __('messages.users.reset_password.subject')
            ]));
        }
    }
}
