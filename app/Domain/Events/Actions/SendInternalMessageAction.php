<?php

namespace App\Domain\Events\Actions;

use Ensi\InternalMessenger\Dto\MessageForCreate;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;
use Spatie\QueueableAction\QueueableAction;
use Throwable;

class SendInternalMessageAction
{
    use QueueableAction;

    /** @throws Throwable */
    public function execute($data): void
    {
        $message = new MessageForCreate($data);
        $topic = topic('communication.command.create-message.1');

        (new HighLevelProducer($topic))->sendOne($message->__toString());
    }
}
