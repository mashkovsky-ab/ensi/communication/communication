<?php

namespace App\Domain\Events\Actions\Orders;

use App\Domain\Events\Actions\Orders\Dto\OrderDto;
use App\Domain\Events\Actions\SendEmailAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\MailTypeEnum;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination as AdminAuthRequestBodyPagination;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\BuClient\Api\OperatorsApi;
use Ensi\BuClient\Dto\RequestBodyPagination as BuRequestBodyPagination;
use Ensi\BuClient\Dto\SearchOperatorsRequest;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Api\ProductsApi;
use Illuminate\Support\Arr;

class AdminOrderMailNotify extends OrderMailNotify
{
    public function __construct(
        protected OrdersApi $ordersApi,
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
        protected OperatorsApi $operatorsApi,
        protected UsersApi $usersApi,
        protected SendEmailAction $sendEmailAction,
        protected ValidateOrderChangedAction $validateOrderChangedAction
    ) {
        parent::__construct(
            $this->ordersApi,
            $this->offersApi,
            $this->productsApi,
            $this->sendEmailAction,
            $this->validateOrderChangedAction
        );
    }

    public function payed(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_PAID, "Заказ №{$order->number} создан и оплачен");
    }

    public function cancelled(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_CANCELED, "Заказ №{$order->number} отменен");
    }

    public function delivered(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_DELIVERED, "Заказ №{$order->number} доставлен");
    }

    /**
     * Список получателей
     * @param OrderDto $orderDto
     * @param Order $order
     * @return array
     */
    protected function getRecipientLetter(OrderDto $orderDto, Order $order): array
    {
        $searchOperatorsRequest = new SearchOperatorsRequest();
        $sellerIds = $this->getSellerIds($order);
        $searchOperatorsRequest->setFilter((object)[
            'seller_id' => $sellerIds,
        ]);
        $searchOperatorsRequest->setPagination(new BuRequestBodyPagination([
            'limit' => -1,
            'offset' => 0,
        ]));
        $operators = $this->operatorsApi->searchOperators($searchOperatorsRequest)->getData();

        $mailReceivers = [];
        $userIds = Arr::pluck($operators, 'user_id');
        if ($userIds) {
            $searchUsersRequest = new SearchUsersRequest();
            $searchUsersRequest->setFilter((object)[
                'id' => $userIds,
                'role' => RoleEnum::SELLER_ADMINISTRATOR,
            ]);
            $searchUsersRequest->setPagination(new AdminAuthRequestBodyPagination([
                'limit' => -1,
                'offset' => 0,
            ]));

            $managers = $this->usersApi->searchUsers($searchUsersRequest)->getData();

            foreach ($managers as $manager) {
                $managerEmail = $manager
                    ? ($manager->getEmail() ?: $manager->getLogin())
                    : '';
                if ($managerEmail) {
                    $mailReceivers [] = $managerEmail;
                }
            }
        }

        return $mailReceivers;
    }

    /**
     * @param Order $order
     * @return array|OrderItem[]|null
     */
    protected function getSellerIds(Order $order): ?array
    {
        return collect($order->getDeliveries())
            ->pluck('shipments')
            ->collapse()
            ->pluck('seller_id')
            ->unique()
            ->values()
            ->all();
    }
}
