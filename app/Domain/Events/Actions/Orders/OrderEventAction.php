<?php

namespace App\Domain\Events\Actions\Orders;

use App\Domain\Events\Actions\Orders\Dto\OrderEventDto;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use RdKafka\Message;

class OrderEventAction
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct(public OrderMailNotify $orderMailNotify, public AdminOrderMailNotify $adminOrderMailNotify)
    {
    }

    public function execute(Message $kafkaMessage)
    {
        $orderEventDto = OrderEventDto::makeFromMessage($kafkaMessage);

        if ($orderEventDto->event == self::UPDATE) {
            $this->updated($orderEventDto);
        }
    }

    public function updated(OrderEventDto $order): void
    {
        $this->notifyIfOrderPaid($order);
        $this->notifyIfOrderCancelled($order);
        $this->notifyIfOrderDelivered($order);
    }

    /**
     * @param OrderEventDto $order
     */
    private function notifyIfOrderPaid(OrderEventDto $order): void
    {
        if (in_array('payment_status', $order->dirty) && $order->attributes->payment_status == PaymentStatusEnum::PAID) {
            $this->orderMailNotify->payed($order->attributes);
            $this->adminOrderMailNotify->payed($order->attributes);
        }
    }

    /**
     * @param OrderEventDto $order
     */
    private function notifyIfOrderCancelled(OrderEventDto $order): void
    {
        if (in_array('status', $order->dirty) && $order->attributes->status == OrderStatusEnum::CANCELED) {
            $this->orderMailNotify->cancelled($order->attributes);
            $this->adminOrderMailNotify->cancelled($order->attributes);
        }
    }

    /**
     * @param OrderEventDto $order
     */
    private function notifyIfOrderDelivered(OrderEventDto $order): void
    {
        if (in_array('status', $order->dirty) && $order->attributes->status == OrderStatusEnum::DONE) {
            $this->orderMailNotify->delivered($order->attributes);
            $this->adminOrderMailNotify->delivered($order->attributes);
        }
    }
}
