<?php

namespace App\Domain\Events\Actions\Orders;

use App\Domain\Events\Actions\Orders\Dto\OrderDto;
use App\Domain\Events\Actions\SendEmailAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\MailTypeEnum;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class OrderMailNotify
{
    public function __construct(
        protected OrdersApi $ordersApi,
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
        protected SendEmailAction $sendEmailAction,
        protected ValidateOrderChangedAction $validateOrderChangedAction
    ) {
    }

    public function payed(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::ORDER_PAID, "Заказ №{$order->number} создан и оплачен");
    }

    public function cancelled(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::ORDER_CANCELLED, "Заказ №{$order->number} отменен");
    }

    public function delivered(OrderDto $order): void
    {
        $this->send($order, MailTypeEnum::ORDER_DELIVERED, "Заказ №{$order->number} доставлен");
    }

    /**
     * @param OrderDto $order
     * @param string $mailType
     * @param string $subject
     */
    protected function send(OrderDto $order, string $mailType, string $subject): void
    {
        try {
            $data = array_merge($this->prepareData($order), ['subject' => $subject]);

            $validData = $this->validateOrderChangedAction->execute($data);

            $allRecipients = collect($validData['to']);

            if (count($allRecipients) > 0) {
                foreach ($allRecipients->chunk(10) as $chunk) {
                    $recipients = array_values($chunk->all());
                    $this->sendEmailAction->onQueue()->execute($recipients, $mailType, $validData);
                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage(), [
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);

            throw $e;
        }
    }

    /**
     * @param OrderDto $orderId
     * @return array
     * @throws ApiException
     */
    protected function prepareData(OrderDto $orderDto): array
    {
        $order = $this->ordersApi->getOrder($orderDto->id, "deliveries.shipments,deliveries.shipments.orderItems")->getData();

        $orderItems = $this->getOrderItems($order);

        $totalCount = 0;
        $itemsCost = 0;
        $items = [];

        $offersIds = collect($orderItems)->pluck('offer_id')->all();
        $productsData = $this->getProductsData($offersIds);

        foreach ($orderItems as $orderItem) {
            $imageUrl = $productsData->get($orderItem->getOfferId())?->getMainImage();

            $items[] = [
                'external_id' => $orderItem->getOfferExternalId(),
                'name' => $orderItem->getName(),
                'image_url' => $imageUrl,
                'price' => price_format(cop2rub($orderItem->getPrice() / $orderItem->getQty())),
                'qty' => (int)$orderItem->getQty(),
                'sum' => price_format(cop2rub($orderItem->getPrice())),
            ];

            $totalCount += $orderItem->getQty();
            $itemsCost += $orderItem->getPrice();
        }

        return [
            'order_number' => $order->getNumber(),
            'create_date' => $order->getCreatedAt()->format('d.m.Y H:i'),
            'receiver_name' => $order->getReceiverName(),
            'receiver_email' => $order->getReceiverEmail(),
            'receiver_phone' => phone_print($order->getReceiverPhone()),
            'delivery_address' => $order->getDeliveryAddress()->getAddressString() ?? '',
            'delivery_comment' => $order->getDeliveryComment() ?? '',
            'delivery_cost' => price_format(cop2rub($order->getDeliveryCost())),
            'basket_items' => $items,
            'items_cost' => price_format(cop2rub($itemsCost)),
            'order_cost' => price_format(cop2rub($order->getCost())),
            'total_count' => (int)$totalCount,
            'to' => $this->getRecipientLetter($orderDto, $order),
        ];
    }

    /**
     * Список получателей
     * @param OrderDto $orderDto
     * @param Order $order
     * @return array
     */
    protected function getRecipientLetter(OrderDto $orderDto, Order $order): array
    {
        return [$orderDto->customer_email];
    }

    /**
     * @param int[] $offersIds
     * @return Collection<int,Product>
     */
    protected function getProductsData(array $offersIds): Collection
    {
        if (!$offersIds) {
            return new Collection();
        }

        $offersRequest = new SearchOffersRequest();
        $offersRequest->setFilter((object)['id' => $offersIds]);

        $offers = collect($this->offersApi->searchOffers($offersRequest)->getData())
            ->mapWithKeys(fn (Offer $offer) => [$offer->getProductId() => $offer->getId()]);

        $productsRequest = new SearchProductsRequest();
        $productsRequest->setFilter((object)['id' => $offers->keys()->all()]);

        return collect($this->productsApi->searchProducts($productsRequest)->getData())
            ->mapWithKeys(fn (Product $product) => [$offers[$product->getId()] => $product]);
    }

    /**
     * @param Order $order
     * @return array|OrderItem[]|null
     */
    protected function getOrderItems(Order $order): ?array
    {
        return collect($order->getDeliveries())
            ->pluck('shipments')->collapse()
            ->pluck('order_items')->collapse()
            ->values()->all();
    }
}
