<?php

namespace App\Domain\Example\Actions;

class SyncHandleKafkaTopicAction extends AbstractHandleKafkaTopicAction
{
    protected function getActionType(): string
    {
        return 'sync';
    }
}
