<?php

namespace App\Domain\Example\Actions;

use App\Domain\Example\Exceptions\ExampleException;
use Ensi\InternalMessenger\Dto\File;
use Ensi\InternalMessenger\ObjectSerializer;
use RdKafka\Message;
use Throwable;

abstract class AbstractHandleKafkaTopicAction
{
    abstract protected function getActionType(): string;

    /** @throws Throwable */
    public function execute(Message $message): void
    {
        $type = $this->getActionType();

        try {
            /** @var File $file */
            $file = ObjectSerializer::deserialize($message->payload, File::class);
            echo "The message was successfully handled $type action:\n"
                . "path={$file->getPath()}\n"
                . "root_path={$file->getRootPath()}\n"
                . "url={$file->getUrl()}\n";
        } catch (Throwable $e) {
            $message = 'An error occurred while converting a message to dto object: ' . $e->getMessage();

            throw new ExampleException($message);
        }
    }
}
