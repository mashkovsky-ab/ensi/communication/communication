<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;

class DeleteChannelAction
{
    public function execute(int $channelId): void
    {
        Channel::destroy($channelId);
    }
}
