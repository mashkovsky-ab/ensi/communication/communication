<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;
use Illuminate\Support\Arr;

class CreateChannelAction
{
    public function execute(array $fields): Channel
    {
        return Channel::create(Arr::only($fields, Channel::FILLABLE));
    }
}
