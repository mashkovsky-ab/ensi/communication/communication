<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Theme;
use Illuminate\Support\Arr;

class CreateThemeAction
{
    public function execute(array $fields): Theme
    {
        return Theme::create(Arr::only($fields, Theme::FILLABLE));
    }
}
