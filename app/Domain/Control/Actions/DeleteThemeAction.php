<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Theme;

class DeleteThemeAction
{
    public function execute(int $themeId): void
    {
        Theme::destroy($themeId);
    }
}
