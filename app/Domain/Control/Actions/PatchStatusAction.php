<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Status;
use Illuminate\Support\Arr;

class PatchStatusAction
{
    public function execute(int $statusId, array $fields): Status
    {
        $status = Status::findOrFail($statusId);
        $status->update($fields);

        return $status;
    }
}
