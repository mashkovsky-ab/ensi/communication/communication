<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;
use Illuminate\Support\Arr;

class PatchChannelAction
{
    public function execute(int $channelId, array $fields): Channel
    {
        $channel = Channel::findOrFail($channelId);
        $channel->update($fields);

        return $channel;
    }
}
