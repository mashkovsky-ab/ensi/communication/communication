<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Status;
use Illuminate\Support\Arr;

class CreateStatusAction
{
    public function execute(array $fields): Status
    {
        return Status::create(Arr::only($fields, Status::FILLABLE));
    }
}
