<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Type;
use Illuminate\Support\Arr;

class CreateTypeAction
{
    public function execute(array $fields): Type
    {
        return Type::create(Arr::only($fields, Type::FILLABLE));
    }
}
