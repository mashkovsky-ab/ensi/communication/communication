<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Type;
use Illuminate\Support\Arr;

class PatchTypeAction
{
    public function execute(int $typeId, array $fields): Type
    {
        $type = Type::findOrFail($typeId);
        $type->update($fields);

        return $type;
    }
}
