<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Theme;
use Illuminate\Support\Arr;

class PatchThemeAction
{
    public function execute(int $themeId, array $fields): Theme
    {
        $theme = Theme::findOrFail($themeId);
        $theme->update($fields);

        return $theme;
    }
}
