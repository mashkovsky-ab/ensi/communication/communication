<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Status;

class DeleteStatusAction
{
    public function execute(int $statusId): void
    {
        Status::destroy($statusId);
    }
}
