<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Type;

class DeleteTypeAction
{
    public function execute(int $typeId): void
    {
        Type::destroy($typeId);
    }
}
