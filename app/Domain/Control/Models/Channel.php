<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 * @package App\Domain\Control\Models
 *
 * @property int $id
 * @property string $name
 */
class Channel extends Model
{
    protected $table = 'channels';

    const FILLABLE = [
        'name',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;
}
