<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 * @package App\Domain\Control\Models
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property bool $default
 * @property array $channel
 */
class Status extends Model
{
    protected $table = 'statuses';
    protected $casts = [
        'channel' => 'array',
    ];

    const FILLABLE = [
        'name', 'active', 'default', 'channel',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;
}
