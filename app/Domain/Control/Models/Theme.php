<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Theme
 * @package App\Domain\Control\Models
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property array $channel
 */
class Theme extends Model
{
    protected $table = 'themes';
    protected $casts = [
        'channel' => 'array',
    ];

    const FILLABLE = [
        'name', 'active', 'channel',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;
}
