<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Control\Models\Status;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StatusesQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Status::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('id');
    }
}
