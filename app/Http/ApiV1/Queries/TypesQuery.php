<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Control\Models\Type;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TypesQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Type::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('id');
    }
}
