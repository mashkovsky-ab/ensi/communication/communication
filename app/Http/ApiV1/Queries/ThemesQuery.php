<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Control\Models\Theme;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ThemesQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Theme::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('id');
    }
}
