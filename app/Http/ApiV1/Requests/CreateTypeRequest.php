<?php

namespace App\Http\ApiV1\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateTypeRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'active' => ['nullable', 'boolean'],
            'channel' => ['nullable', 'array'],
            'channel.*' => ['integer'],
        ];
    }
}
