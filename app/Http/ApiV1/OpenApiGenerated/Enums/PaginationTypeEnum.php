<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

class PaginationTypeEnum
{
    public const CURSOR = 'cursor';
    public const OFFSET = 'offset';

    /**
     * @return string[]
     */
    public static function cases(): array
    {
        return [
            self::CURSOR,
            self::OFFSET,
        ];
    }
}
