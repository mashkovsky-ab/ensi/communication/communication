<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Control\Actions\CreateChannelAction;
use App\Domain\Control\Actions\DeleteChannelAction;
use App\Domain\Control\Actions\PatchChannelAction;
use App\Http\ApiV1\Requests\CreateChannelRequest;
use App\Http\ApiV1\Requests\PatchChannelRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;
use App\Http\ApiV1\Queries\ChannelsQuery;
use App\Http\ApiV1\Resources\ChannelsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class ChannelController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ChannelsQuery $query)
    {
        return ChannelsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateChannelRequest $request, CreateChannelAction $action)
    {
        return new ChannelsResource($action->execute($request->validated()));
    }

    public function patch(int $statusId, PatchChannelRequest $request, PatchChannelAction $action)
    {
        return new ChannelsResource($action->execute($statusId, $request->validated()));
    }

    public function delete(int $statusId, DeleteChannelAction $action)
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
