<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Control\Actions\CreateThemeAction;
use App\Domain\Control\Actions\DeleteThemeAction;
use App\Domain\Control\Actions\PatchThemeAction;
use App\Http\ApiV1\Requests\CreateThemeRequest;
use App\Http\ApiV1\Requests\PatchThemeRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;
use App\Http\ApiV1\Queries\ThemesQuery;
use App\Http\ApiV1\Resources\ThemesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class ThemeController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ThemesQuery $query)
    {
        return ThemesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateThemeRequest $request, CreateThemeAction $action)
    {
        return new ThemesResource($action->execute($request->validated()));
    }

    public function patch(int $themeId, PatchThemeRequest $request, PatchThemeAction $action)
    {
        return new ThemesResource($action->execute($themeId, $request->validated()));
    }

    public function delete(int $themeId, DeleteThemeAction $action)
    {
        $action->execute($themeId);

        return new EmptyResource();
    }
}
