<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Control\Actions\CreateStatusAction;
use App\Domain\Control\Actions\DeleteStatusAction;
use App\Domain\Control\Actions\PatchStatusAction;
use App\Http\ApiV1\Requests\CreateStatusRequest;
use App\Http\ApiV1\Requests\PatchStatusRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;
use App\Http\ApiV1\Queries\StatusesQuery;
use App\Http\ApiV1\Resources\StatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class StatusController
{
    public function search(PageBuilderFactory $pageBuilderFactory, StatusesQuery $query)
    {
        return StatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateStatusRequest $request, CreateStatusAction $action)
    {
        return new StatusesResource($action->execute($request->validated()));
    }

    public function patch(int $statusId, PatchStatusRequest $request, PatchStatusAction $action)
    {
        return new StatusesResource($action->execute($statusId, $request->validated()));
    }

    public function delete(int $statusId, DeleteStatusAction $action)
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
