<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Control\Actions\CreateTypeAction;
use App\Domain\Control\Actions\DeleteTypeAction;
use App\Domain\Control\Actions\PatchTypeAction;
use App\Http\ApiV1\Requests\CreateTypeRequest;
use App\Http\ApiV1\Requests\PatchTypeRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Queries\TypesQuery;
use App\Http\ApiV1\Resources\TypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class TypeController
{
    public function search(PageBuilderFactory $pageBuilderFactory, TypesQuery $query)
    {
        return TypesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateTypeRequest $request, CreateTypeAction $action)
    {
        return new TypesResource($action->execute($request->validated()));
    }

    public function patch(int $typeId, PatchTypeRequest $request, PatchTypeAction $action)
    {
        return new TypesResource($action->execute($typeId, $request->validated()));
    }

    public function delete(int $typeId, DeleteTypeAction $action)
    {
        $action->execute($typeId);

        return new EmptyResource();
    }
}
