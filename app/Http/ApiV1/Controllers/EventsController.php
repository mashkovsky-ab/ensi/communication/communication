<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Events\Actions\ParseEventAction;
use App\Http\ApiV1\Requests\EventRequest;

class EventsController
{
    public function parse(EventRequest $request, ParseEventAction $action)
    {
        $action->execute($request);

        return response()->noContent(200);
    }
}
