<?php

return [
    'communication' => [
        'internal-messenger' => [
            'base_uri' => env('INTERNAL_MESSAGES_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'orders' => [
        'oms' => [
            'base_uri' => env('OMS_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'catalog' => [
        'pim' => [
            'base_uri' => env('PIM_SERVICE_HOST') . '/api/v1',
        ],
        'offers' => [
            'base_uri' => env('OFFERS_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'units' => [
        'bu' => [
            'base_uri' => env('BU_SERVICE_HOST') . '/api/v1',
        ],
        'admin-auth' => [
            'base_uri' => env('ADMIN_AUTH_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'customers' => [
        'customer-auth' => [
            'base_uri' => env('CUSTOMER_AUTH_SERVICE_HOST') . '/api/v1',
        ],
        'customers' => [
            'base_uri' => env('CUSTOMERS_SERVICE_HOST') . '/api/v1',
        ],
    ],
];
