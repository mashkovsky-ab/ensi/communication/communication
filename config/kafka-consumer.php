<?php

use App\Domain\Events\Actions\Customers\CustomerAuth\CustomerAuthGeneratePasswordTokenAction;
use App\Domain\Events\Actions\Customers\CustomerAuth\SendMail\UserUpdatedAction;
use App\Domain\Events\Actions\Customers\Customers\ChangeEmailAction;
use App\Domain\Events\Actions\Customers\Customers\SendMail\CustomerUpdatedAction;
use App\Domain\Events\Actions\Orders\OrderEventAction;
use App\Domain\Events\Actions\Units\AdminAuth\SendEmail\DeactivatedAction;
use App\Domain\Events\Actions\Units\AdminAuth\SendEmail\SettingPasswordAction;
use App\Domain\Events\Actions\Units\AdminAuth\SendEmail\UpdatedAction;
use App\Domain\Events\Actions\Customers\ResetPasswordAction;
use App\Domain\Events\Actions\Customers\ResetPasswordSuccessAction;

return [
    'processors' => [
        [
            /*
            | Optional, defaults to `null`.
            | Here you may specify which topic should be handled by this processor.
            | Processor handles all topics by default.
            */
            'topic' => topic('communication.test.example.1'),

            /*
            | Optional, defaults to `null`.
            | Here you may specify which ensi/laravel-phprdkafka consumer should be handled by this processor.
            | Processor handles all consumers by default.
            */
            'consumer' => 'default',

            /*
            | Optional, defaults to `action`.
            | Here you may specify processor's type. Defaults to `action`
            | Supported types:
            |  - `action` - a simple class with execute method;
            |  - `job` - Laravel Queue Job. It will be dispatched using `dispatch` or `dispatchSync` method;
            */
            'type' => 'action',

            /*
            | Required.
            | Fully qualified class name of a processor class.
            */
            // for async action test
            //'class' => \App\Domain\Example\Actions\AsyncHandleKafkaTopicAction::class,
            // for sync action test
            'class' => \App\Domain\Example\Actions\SyncHandleKafkaTopicAction::class,

            /*
            | Optional, defaults to `false`.
            | Proxy messages to Laravel's queue.
            | Supported values:
            |  - `false` - do not stream message. Execute processor in syncronous mode;
            |  - `true` - stream message to Laravel's default queue;
            |  - `<your-favorite-queue-name-as-string>` - stream message to this queue;
            */
            // for async action test
            //'queue' => true,
            // for sync action test
            'queue' => false,

            /*
            | Optional, defaults to 5000.
            | Kafka consume timeout in milliseconds .
            */
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('admin-auth.fact.generated-password-token.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => SettingPasswordAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customer-auth.fact.password-reset.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => ResetPasswordAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customer-auth.fact.password-reset-success.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => ResetPasswordSuccessAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customer-auth.fact.generated-password-token.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => CustomerAuthGeneratePasswordTokenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('admin-auth.fact.deactivated-user.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => DeactivatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('admin-auth.fact.updated-user.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => UpdatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('orders.fact.orders.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => OrderEventAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customers.fact.changes-email.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => ChangeEmailAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customer-auth.fact.user-updated.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => UserUpdatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => topic('customers.fact.customer-updated.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => CustomerUpdatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],
];
